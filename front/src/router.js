import Vue from 'vue'
import Router from 'vue-router'

import pageNotFound from './components/PageNotFound'
import home from './components/HomePage'
import searchPage from './components/SearchPage'
import logPage from './components/LogPage'
import signPage from './components/SignPage'
import watchPage from './components/WatchPage'
import profilPage from './components/ProfilPage'
import playlistsPage from './components/PlaylistsPage'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: home
        },
        {
            path: `/search`,
            name: 'search',
            component: searchPage
        },
        {
            path: `/log`,
            name: 'log',
            component: logPage,
            beforeEnter(to, from, next) {
                if (window.$cookies.isKey('mixHamuser')) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: `/sign`,
            name: 'sign',
            component: signPage,
            beforeEnter(to, from, next) {
                if (window.$cookies.isKey('mixHamuser')) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: `/watch`,
            name: 'watch',
            component: watchPage,
            props: (route) => ({...route.params})
        },
        {
            path: `/profil`,
            name: 'profil',
            component: profilPage,
            beforeEnter(to, from, next) {
                if (!window.$cookies.isKey('mixHamuser')) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: `/playlists`,
            name: 'playlists',
            component: playlistsPage,
            beforeEnter(to, from, next) {
                if (!window.$cookies.isKey('mixHamuser')) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: `*`,
            name: '404',
            component: pageNotFound
        }
    ]
});