import 'bootstrap';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import BootstrapVue from 'bootstrap-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import moment from 'moment';
import axios from "axios";
import cookies from "vue-cookies";
import VuePlyr from "vue-plyr";
import VueGlobalVar from "vue-global-var";
import Playlist from './model/Playlist';
import PlaylistList from './model/PlaylistList';

library.add(fas,far);
Vue.use(BootstrapVue);
Vue.use(cookies);
Vue.use(VuePlyr);
Vue.component('icon', FontAwesomeIcon);
Vue.config.productionTip = false

Vue.filter('formatDate', function(value) {
    if (value) {
        let userLang = navigator.language || navigator.userLanguage;
        moment.locale(userLang);
        return moment(String(value)).format('LLLL');
    }
})

Vue.$cookies.config('1d');

const http = axios.create({
    baseURL: process.env.VUE_APP_BACKEND_URL,
});

Vue.use(VueGlobalVar, {
    globals: {
        $favoris: new Playlist(),
        $playlists: new PlaylistList(),
        $history: new Playlist()
    }
});

Vue.prototype.$http = http;

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
