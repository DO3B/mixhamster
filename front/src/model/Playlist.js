class Playlist {
    constructor() {
        this._id = '';
        this.title = '';
        this.userId = '';
        this.videos = [];
    }
  
    set(playlist) {
      Object.assign(this, playlist);
    }
  }
  export default Playlist;