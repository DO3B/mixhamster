module.exports = {
    outputDir: '../pilote/public/',
    transpileDependencies: ['vue-plyr']
}

process.env.VUE_APP_BACKEND_URL = process.env.NODE_ENV === 'production' ? "https://mixhamster.herokuapp.com/" : 'http://0.0.0.0:8080/'