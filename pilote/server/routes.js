const express = require('express');

var router = express.Router();
const Token = require('../server/token');


router.use(function (request, response, next) {
    let token = request.headers["x-auth-token"];
    const noAuthRoutes = ["/users/", "/users/register", "/videos/search", "/videos/getThis", "/videos/getAll"];
    if(!noAuthRoutes.includes(request.path) && request.method != "OPTIONS"){
        if(token){
            Token.checkValidity(token).then(function(res){
                request.body.token = res;
                next();
            }).catch(function(error){
                let result = {
                    success: false,
                    error: "Token invalid"
                };
                return response.status(403).send(result);
            });
        } else {
            let result = {
                success: false,
                error: "We need a token to pursue, please login"
            };
            return response.status(401).send(result);
        }
    } else {
        next();
    }

});

router.use('/users', require('../routes/users-routes'));
router.use('/videos', require('../routes/videos-routes.noauth'));
router.use('/playlists', require('../routes/playlists-routes'));
router.use('/videos', require('../routes/videos-routes'));

module.exports = router;
