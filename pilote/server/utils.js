const fs = require('fs'),
    axios = require('axios');

module.exports = {
    download_image: function (url, image_path) {
        axios({ url, responseType: 'stream' }).then(response => {
            new Promise((resolve, reject) => {
                response.data.pipe(fs.createWriteStream(image_path))
                    .on('finish', () => resolve())
                    .on('error', e => reject(e));
            });
        });
    }
};
