const express = require('express'),
      fs = require('fs'),
      helmet = require('helmet'),
      http = require('http'),
    //   https = require('https'),
      morgan = require('morgan');

const routes = require('./routes');
const {serverConfig} = require('../config/config');

const app = express();

app.use(morgan('tiny'));
app.use(helmet());
app.use(express.json());
app.enable('trust proxy');
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-with, Content-Type, Authorization, x-auth-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
  
app.use('/api', routes);
app.use(express.static('./public'));

module.exports = {
    startServer: function(host, port){
        app.set('port', process.env.PORT || port);
        app.set('host', process.env.HOST || "0.0.0.0");

        let options = {
            // key: fs.readFileSync(serverConfig.server.key),
            // cert: fs.readFileSync(serverConfig.server.cert)
        };

        http.createServer(options, app).listen(app.get('port'), app.get('host'), function() {
            console.log('Express server listening on port ' + app.get('port'));
        });
    }
}