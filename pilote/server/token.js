const jwt = require('jsonwebtoken');

const {serverConfig} = require('../config/config');

module.exports = {
    createToken: function(id, mail){
        let payload = {
            id: id,
            mail: mail
        };

        return jwt.sign(payload, serverConfig.token.secret, {expiresIn: serverConfig.token.expiresIn});
    },

    checkValidity: function(token){
        return new Promise(function(resolve, reject){
            jwt.verify(token, serverConfig.token.secret, function(err, decoded){
                if (err){
                    reject(err);
                } else {
                    resolve(decoded);
                }
            });
        });
    }
}

