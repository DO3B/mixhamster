const express = require('express'),
    argon2 = require('argon2'),
    axios = require('axios').default;

const token = require('../server/token');
var router = express.Router();

const { silosConfig } = require('../config/config');

const usersSilos = silosConfig.silos.users;

router.post('/', function (request, response) {
    axios.post('http://' + usersSilos.host + ':' + usersSilos.port + usersSilos.endpoint + "/login", {
        mail: request.body.mail,
        password: request.body.password
    }).then(function (res) {
        let user = {
            pseudo: res.data.user.pseudo,
            mail: res.data.user.mail
        }; 
        let result = {
            success: true,
            user: user,
        };

        // Dans le cas d'une modification du compte, le token est déjà présent, du coup pas besoin d'en refaire un
        if(!request.headers["x-auth-token"] || request.headers["x-auth-token"] === undefined){
            result.token = token.createToken(res.data.user._id, user.mail);
        }

        response.status(201).json(result);
    }).catch(function (error) {
        let result = {
            success: false,
            error: error.response.data.message
        };
        response.status(418).json(result);
    });
});

router.post('/register', async function (request, response) {
    let user = {
        pseudo: request.body.pseudo,
        mail: request.body.mail,
        password: request.body.password
    }
    if (!user.pseudo || !user.mail || !user.password) {
        let result = {
            success: false,
            message: "L'utilisateur n'a pas été bien défini, merci de respecter la syntaxe"

        };
        response.status(404).json(result)
    } else {
        try {
            user.password = await argon2.hash(user.password);
        } catch (err) {
            let result = {
                success: false,
                message: "La vraie erreur"
    
            };
            response.status(500).json(result)
        }
        axios.post('http://' + usersSilos.host + ':' + usersSilos.port + usersSilos.endpoint + "/register", {
            pseudo: user.pseudo,
            mail: user.mail,
            password: user.password
        }).then(function (res) {
            let result = {
                success: true,
                user: res.data.user
            };
            response.status(200).json(result)
        }).catch(function (error) {
            let result = {
                success: false,
                message: error.response.data.message
            };
            response.status(418).json(result)
        });
    }
});

router.post('/update', async function (request, response) {
    let user = {
        pseudo: request.body.pseudo,
        mail: request.body.mail,
        password: request.body.password
    }

    if (!user.pseudo || !user.mail || !user.password || !request.body.token) {
        let result = {
            success: false,
            message: "L'utilisateur n'a pas été bien défini, merci de respecter la syntaxe"

        };
        response.status(404).json(result)
    } else {
        try {
            user.password = await argon2.hash(user.password);
        } catch (err) {
            let result = {
                success: false,
                message: "La vraie erreur"
    
            };
            response.status(500).json(result)
        }
        axios.put('http://' + usersSilos.host + ':' + usersSilos.port + usersSilos.endpoint + "/update", {
            _id: request.body.token.id,
            pseudo: user.pseudo,
            mail: user.mail,
            password: user.password
        }).then(function (res) {
            let result = {
                success: true,
                user: res.data.user
            };
            if(res.data.user.mail != request.body.token.mail){
                result.token = token.createToken(res.data.user._id, res.data.user.mail);
            }
            response.status(200).json(result)
        }).catch(function (error) {
            let result = {
                success: false,
                message: error.response.data.message
            };
            response.status(418).json(result)
        });
    }
});

module.exports = router;
