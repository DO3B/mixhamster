const express = require('express'),
    axios = require('axios').default;

const { silosConfig } = require('../config/config');

const videoSilos = silosConfig.silos.videos;

var router = express.Router();

router.post("/search", (request, response) => {
    if (request.body.keyword) {
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/video/searchvideos/" + request.body.keyword).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error);
        });
    }

});

router.post('/getAll', function (req, res) {
    axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/video/getall/").then(function(response){
        res.status(200).json({ success: true, videos: response.data.videos });
    }).catch(function (err) {
        res.status(500).json({ success: false, error: err });
    });
});

router.post('/getThis', function (req, res) {
    if(req.body.video){
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/video/getthis/", {
            title: req.body.video.title,
            author: req.body.video.author
        }).then(function(response) {
            res.status(200).json({ success: true, video: response.data.video });
        }).catch(function (err) {
            res.status(500).json({ success: false, error: err.response.data });
        });
    } else {
        res.status(418).json({ success: false, error: "Title or author were not found"});
    }
    
});

module.exports = router;
