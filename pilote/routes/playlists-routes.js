const express = require('express'),
      axios = require('axios').default;

const { silosConfig } = require('../config/config');

const videoSilos = silosConfig.silos.videos;

var router = express.Router();

router.post("/getAll", (request, response) => {
    if (request.body.token) {
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/getall",{
            user: {
                _id: request.body.token.id
            }
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error);
        });
    }
});

router.post("/getFavorite", (request, response) => {
    if (request.body.token) {
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/getfavorite",{
            user: {
                _id: request.body.token.id
            }
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error);
        });
    }
});

router.post("/getHistory", (request, response) => {
    if (request.body.token) {
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/gethistory",{
            user: {
                _id: request.body.token.id
            }
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error);
        });
    }
});

router.post("/create", (request, response) => {
    if (request.body.token && request.body.playlist) {
        let playlist = request.body.playlist;
        playlist.userId = request.body.token.id;
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/create",{
            playlist: playlist
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error);
        });
    } else {
        let result = {
            success: false,
            error: "We need a playlist to pursue"
        };
        return response.status(418).send(result);
    }
});

router.put("/delete", (request, response) => {
    if (request.body.playlist) {
        let playlist = request.body.playlist;
        playlist.userId = request.body.token.id;
        axios.put('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/delete",{
            playlist: playlist
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error.response.data);
        });
    } else {
        let result = {
            success: false,
            error: "We need a playlist to pursue"
        };
        return response.status(418).send(result);
    }
});

router.put("/add", (request, response) => {
    if (request.body.playlist && request.body.video) {
        let playlist = request.body.playlist;
        playlist.userId = request.body.token.id;
        axios.put('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/add",{
            playlist: playlist,
            video: request.body.video
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error.response.data);
        });
    } else {
        let result = {
            success: false,
            error: "We need a playlist and a video to pursue"
        };
        return response.status(418).send(result);
    }
});

router.put("/remove", (request, response) => {
    if (request.body.playlist && request.body.video) {
        let playlist = request.body.playlist;
        playlist.userId = request.body.token.id;
        axios.put('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/playlist/remove",{
            playlist: playlist,
            video: request.body.video
        }).then(function (res) {
            response.status(200).json(res.data);
        }).catch(function (error) {
            response.status(500).json(error.response.data);
        });
    } else {
        let result = {
            success: false,
            error: "We need a playlist and a video to pursue"
        };
        return response.status(418).send(result);
    }
});

module.exports = router;