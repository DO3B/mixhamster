const express = require('express'),
    axios = require('axios').default,
    fs = require('fs'),
    path = require('path'),
    ytdl = require('ytdl-core');

const { silosConfig } = require('../config/config');
const { serverConfig } = require('../config/config');

const videoSilos = silosConfig.silos.videos;
const {download_image} = require('../server/utils');

var router = express.Router();

router.post('/add', function (request, response) {
    if(request.body.video){
        let video = request.body.video;
        if(video.platform == "youtube" && !video.link.includes(serverConfig.server.host)){
            const videosFolder = process.cwd() + '/public/videos';
            const videoFilename = video._id + ".mp4";
            download_image(video.thumbnailLink, path.join(videosFolder, video._id + ".png"));
            ytdl(video.link).pipe(fs.createWriteStream(path.join(videosFolder, videoFilename)));
            video.link = "https://" + serverConfig.server.host + ":" + serverConfig.server.port + "/videos/" + video._id + ".mp4";
            video.thumbnailLink = "https://" + serverConfig.server.host + ":" + serverConfig.server.port + "/videos/" + video._id + ".png";
        }
        axios.post('http://' + videoSilos.host + ':' + videoSilos.port + videoSilos.endpoint + "/video/add/",{
            video: request.body.video
        }).then(function (res) {
            response.status(200).json({ success: true, video: res.data.video });
        }).catch(function (err) {
            if(err.response.status == 418){
                response.status(409).json({success: false, error: err.response.data.message});
            }
            response.status(500).json({ success: false, error: err.response.data });
        });
    } else {
        response.status(418).json({success: false, error: "Video was not created"});
    }
});


module.exports = router;
