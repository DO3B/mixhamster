const {startServer} = require('./server/server');
const {serverConfig} = require('./config/config');

startServer(serverConfig.server.host, serverConfig.server.port);