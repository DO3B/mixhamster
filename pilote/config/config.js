const fs = require('fs'),
      path = require('path');

const silosConfig = "silos-config.json";
const serverConfigPath = "server-config.json";

function importConf(filepath){
    let conf;
    try {
        conf = fs.readFileSync(path.join(__dirname, filepath));
        return JSON.parse(conf);
    } catch( error ){
        console.log("Couldn't find " + filepath + ", I will use the default config file.");
        conf = fs.readFileSync(path.join(__dirname, filepath + ".default"));
        return JSON.parse(conf);
    }
}

module.exports.silosConfig = importConf(silosConfig);
module.exports.serverConfig = importConf(serverConfigPath);
