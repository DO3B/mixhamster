# mixHamster

**mixHamster** est une application permettant de visualiser des vidéos de différentes plateformes de streaming (YouTube et RuTube). Cette application propose aussi de créer un compte afin de créer, modifier et supprimer des Playlists.

**<u>/!\\</u> Depuis la mise à jour de RuTube du 26 décembre 2019, une authentification sur leur plateforme est requise afin de pouvoir regarder leurs vidéos.**

## Heroku

>  Attention ! Il est nécessaire de cliquer au moins une fois sur les silos afin de lancer les différents services.

La plateforme est disponible aux liens suivant :
*  [http://mixhamster-users.herokuapp.com](http://mixhamster-users.herokuapp.com), le silo utilisateurs,
*  [http://mixhamster-video.herokuapp.com](http://mixhamster-video.herokuapp.com), le silo vidéos,
*  [http://mixhamster.herokuapp.com](http://mixhamster.herokuapp.com), l'application mixHamster.

## Architecture 

| Architecture de l'application
|---|
|  ![Essai](architecture.png) |

## Méthodes à développer

### mixHamster

|Fonctions|
|---|
|login|
|logout|
|signIn|

### Silo Vidéos/Playlists

|Fonctions|
|---|
|createPlaylist|
|deletePlaylist|
|getPlaylists| 
|addVideo|
|deleteVideo|
|getVideos|

### Silo Utilisateur

|Fonctions|
|---|
|createUser|
|findUserByEmail|
