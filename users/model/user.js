const mongoose = require('mongoose');
const { databaseConfig } = require('../config/config');

mongoose.connect(databaseConfig.url, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });


const userSchema = new mongoose.Schema({
    pseudo : String,
    mail : {type: String, unique: true, required: true, dropDups: true},
    password: String
  });

var User = mongoose.model('user', userSchema);

module.exports= {
  createUser: function(user) {
    let newUser = new User({
      pseudo: user.pseudo,
      mail: user.mail,
      password: user.password
    });

    return new Promise(function(resolve, reject){
      newUser.save().then(function(response){
        resolve(response);
      }).catch(function(err){
        reject(err);
      });
    });
  },  
  
  findUserByMail: function(mail){
      if (!mail) {
          return new Error("INVALID_EMAIL");
      }
      return User.findOne({ mail: (mail) });
  },

  updateUser: function(user){
    let updateUser = new User(user);
    updateUser._id = user._id;
    updateUser.isNew = false;

    return new Promise(function(resolve, reject){
      updateUser.save().then(function (response) {
        resolve(response);
      }).catch(function (err) {
        reject(err);
      });
    });
  }
}


