const express = require('express'),
      http = require('http');

const app = express();

const routes = require('./routes');

app.use(express.json());
app.use('/api', routes);


module.exports = {
    startServer: function(host, port){
        app.set('port', process.env.PORT || port);
        app.set('host', process.env.HOST || host);
        http.createServer(app).listen(app.get('port'), app.get('host'), function() {
            console.log('Le silo Utilisateurs est à votre écoute sur le port ' + app.get('port'));
        });
    }
}
