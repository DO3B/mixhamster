const express = require('express');

let router = express.Router();

router.use('/users', require('../routes/user-routes'));

module.exports = router;
