const express = require("express"),
      argon2 = require('argon2');

//const bcrypt = require("bcrypt");
const {createUser, findUserByMail, updateUser} = require("../model/user")

var router = express.Router();

router.post('/register', function (req, res) {
    let user = {
        pseudo: req.body.pseudo,
        mail: req.body.mail,
        password: req.body.password
    }

    // check params
    if (!user.pseudo || !user.mail || !user.password) {
        res.status(401).send();
    }
    findUserByMail(user.mail).then(function (response) {
        if (response != null) {
            let result = {
                success: false,
                message: "Mail déjà utilisé"
            };
            res.status(500).json(result);
        } else {
            createUser(user).then(function (response) {
                let result = {
                    success: true,
                    user:{
                        _id: response._id,
                        pseudo: response.pseudo,
                        mail: response.mail,
                        password: response.password
                    }
                };
                res.status(200).json(result);
            }).catch(function (err) {
                res.status(500).send(err)
            });

        }
    });

});

router.post('/login', function (req, res) {
    let user = {
        mail: req.body.mail,
        password: req.body.password
    }
    findUserByMail(user.mail).then(async function (response) {
        if (response === null) {
            let result = {
                success: false,
                message: "Utilisateur non trouvé"
            };
            res.status(500).json(result);
        } else {
            let verifPassword = await argon2.verify(response.password, user.password);
            if (verifPassword) {
                let result = {
                    success: true,
                    user:{
                        _id: response._id,
                        pseudo: response.pseudo,
                        mail: response.mail
                    }
                };
                res.status(200).json(result);
            } else {
                let result = {
                    success: false,
                    message : "La vraie erreur"
                    
                };
                res.status(500).json(result)
            }
        }

    }).catch(function (response) {
        let result = {
            success: false,
            message: "Erreur"
        };
        res.status(500).json(result);
    });
});

router.put('/update', function(req, res){
    let user = {
        _id: req.body._id,
        pseudo: req.body.pseudo,
        mail: req.body.mail,
        password: req.body.password
    };
    
    updateUser(user).then(function(response){   
        let result = {
            success: true,
            user: {
                pseudo: response.pseudo,
                mail: response.mail
            }
        };
        res.status(200).json(result);
    }).catch(function (error){
        let result = {
            success: false,
            message: "Could not update the user"
        };
        if(error.code = 11000){
            result.message = "Cette adresse mail est déjà utilisée."
        }
        res.status(500).json(result);
    });
});

module.exports = router;