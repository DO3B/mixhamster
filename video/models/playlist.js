const mongoose = require('mongoose');
const { videoSchema } = require('./video');
const { databaseConfig } = require('../config/config');

mongoose.connect(databaseConfig.url, { useNewUrlParser: true, useUnifiedTopology: true });

let schema = new mongoose.Schema({
  title: String,
  userId: mongoose.Types.ObjectId,
  videos: [videoSchema]
});

var Playlist = mongoose.model('playlists', schema);

module.exports = {
  createPlaylist: function (playlist) {
    let newplaylist = new Playlist({
      title: playlist.title,
      userId: playlist.userId,
      videos: []
    })
    return new Promise(function (resolve, reject) {
      newplaylist.save().then(function (response) {
        resolve(response);
      }).catch(function (err) {
        reject(err);
      });
    });

  },

  deletePlaylist: function (playlist) {
    return new Promise(function (resolve, reject) {
      Playlist.deleteOne({
        _id: mongoose.Types.ObjectId(playlist._id)
      }).then(function (response) {
        resolve(response);
      }).catch(function (err) {
        reject(err);
      });
    });

  },

  getAll: function (user) {
    return Playlist.find({ title: { $nin: ['Favoris', 'Historique'] }, userId: user._id });
  },

  getFavorite: function (user) {
    return Playlist.findOne({ title: 'Favoris', userId: user._id });
  },

  getHistory: function (user) {
    return Playlist.findOne({ title: 'Historique', userId: user._id });
  },

  addVideoInto: function (playlist, video) {
    return new Promise(function (resolve, reject) {
      let newPlaylist = new Playlist(playlist);

      newPlaylist.isNew = false;
      newPlaylist.videos.push(video);

      newPlaylist.save().then(function (response) {
        resolve(response);
      }).catch(function (err) {
        reject(err);
      });
    });
  },

  removeVideoFrom: function (playlist, video) {
    return new Promise(function (resolve, reject) {
      let newPlaylist = new Playlist(playlist);

      newPlaylist.isNew = false;
      let index = newPlaylist.videos.findIndex(function (item, i) {
        return item._id == video._id
      });
      if (index > -1) {
        newPlaylist.videos.splice(index, 1);
        newPlaylist.save().then(function (response) {
          resolve(response);
        }).catch(function (err) {
          reject(err);
        });
      }
      else {
        reject('Vidéo inexistante');
      }
    });
  }

};
