const mongoose = require('mongoose');
const { databaseConfig } = require('../config/config');

mongoose.connect(databaseConfig.url, { useNewUrlParser: true, useUnifiedTopology: true });

let schema = new mongoose.Schema({ 
    title: String,
    description: String,
    author: String,
    date: String,
    link: {type: String, unique: true, required: true, dropDups: true},
    thumbnailLink: String,
    platform: String
});

var Video = mongoose.model('videos', schema);

module.exports = {
    createVideo: function(video) {
        let newVideo = new Video({
            title: video.title,
            description: video.description,
            author: video.author,
            date: video.date,
            link: video.link,
            thumbnailLink: video.thumbnailLink,
            platform: video.platform
        })
        return new Promise(function(resolve, reject){
          newVideo.save().then(function(response){
            resolve(response);
          }).catch(function(err){
            reject(err);
          });
        });
      
      },

      findAll: function(){
        return Video.find();
      },
      
      findVideoByTitleAndAuthor: function(title,author){
        if (title == null || author == null) {
            return new Error("Invalid field ! Call the police !");
        }
        return Video.findOne({ title: (title), author: (author) });
      }
  
    
};

module.exports.videoSchema = schema;
