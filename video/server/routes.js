const express = require('express');

let router = express.Router();

router.use('/video', require('../routes/video-routes'));
router.use('/playlist', require('../routes/playlist-routes'));

module.exports = router;