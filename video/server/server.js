const express = require('express'),
      http = require('http');

const app = express();

const routes = require('./routes');
const {serverConfig} = require('../config/config');

app.use(express.json());
app.use('/api', routes);


module.exports = {
    startServer: function(host, port){
        app.set('port', process.env.PORT || port);

        http.createServer(app).listen(app.get('port'), host, function() {
            console.log('Le silo Vidéos et PlayLists est à votre écoute sur le port ' + app.get('port'));
        });
    }
}
