const axios = require('axios');
const APIKey = 'AIzaSyC97tWmQisNcWgzByT4K8Eb9A5ZawW9DO8';

module.exports = {
    videoSearch : async function(keyword){
        try {
            let result = {youtube:[], rutube:[]};
            let rep = '';

            rep = await axios.get('https://www.googleapis.com/youtube/v3/search?type=video&part=snippet&maxResults=10&q='+ keyword +'&key=' + APIKey);
            
            //Parsing
            rep.data.items.forEach(function(video) {
                let vid = { _id: video.id.videoId, title: video.snippet.title, description: video.snippet.description, author: video.snippet.channelTitle,
                            date: video.snippet.publishedAt, link: 'http://www.youtube.com/embed/' + video.id.videoId,
                            thumbnailLink: video.snippet.thumbnails.medium.url, platform: 'youtube'};
                result.youtube.push(vid);
            });

            rep = await axios.get('https://rutube.ru/api/search/video/?format=json&query=' + keyword);
            
            //Parsing
            rep.data.results.forEach(function(video) {
                let vid = { _id: video.id, title: video.title, description: video.description, author: video.author.name,
                            date: video.publication_ts, link: 'https://rutube.ru/play/embed/' + video.id,
                            thumbnailLink: video.thumbnail_url, platform: 'rutube'};
                result.rutube.push(vid);
            });

            return result;
        } catch (error) {
            throw(error);
        }
    }
}