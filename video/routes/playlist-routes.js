const { createPlaylist, deletePlaylist, getAll, getFavorite, getHistory, addVideoInto, removeVideoFrom } = require("../models/playlist");
var express = require('express');
var ObjectID = require('mongodb').ObjectID;
let router = express.Router();

router.get('/', function (req, res) {
});

router.post('/getall', function (req, res) {
    getAll(req.body.user).then(function (playlists) {
        res.status(200).json({success: true, playlists: playlists});
    }).catch(function (err) {
        res.status(500).json({success: false, error: err})
    });
});

router.post('/getfavorite', function (req, res) {
    getFavorite(req.body.user).then(function (playlist) {
        res.status(200).json({success: true, playlist: playlist});
    }).catch(function (err) {
        res.status(500).json({success: false, error: err})
    });
});

router.post('/gethistory', function (req, res) {
    getHistory(req.body.user).then(function (playlist) {
        res.status(200).json({success: true, playlist: playlist});
    }).catch(function (err) {
        res.status(500).send({success: false, error: err})
    });
});

router.post('/create', function (req, res) {
    createPlaylist(req.body.playlist).then(function (response) {
        res.status(200).json({success: true, playlist: response})
    }).catch(function (err) {
        res.status(500).json({success: false, error: err})
    });
});

router.put('/delete', function (req, res) {
    deletePlaylist(req.body.playlist).then(function (response) {
        res.status(200).json({success: true, playlist: response})
    }).catch(function (err) {
        res.status(500).json({success: false, error: err})
    });
});

router.put('/add', function (req, res) {
    let video = req.body.video;
    video._id = ObjectID.createFromHexString(video._id);
    addVideoInto(req.body.playlist, video).then(function (response) {
        res.status(200).json({success: true, playlist: response})
    }).catch(function (err) {
        res.status(500).json({success: false, error: err})
    });
});

router.put('/remove', function (req, res) {
    removeVideoFrom(req.body.playlist, req.body.video).then(function (response) {
        res.status(200).json({success: true, playlist: response})
    }).catch(function (err) {
        res.status(500).json({success: false, error: err})
    });
});

module.exports = router;
