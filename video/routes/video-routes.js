const { findAll, findVideoByTitleAndAuthor, createVideo } = require("../models/video");
const { videoSearch } = require('../server/search');
const express = require('express');
let router = express.Router()

router.post('/searchvideos/:keyword', function (req, res) {
    videoSearch(req.params.keyword).then(async function (ret) { 
        let rutube = [];
        for(var video of ret.rutube){
            try {
                let research = await findVideoByTitleAndAuthor(video.title, video.author);
                if(research){
                    video = research;
                    rutube.push(video)
                } else {
                    rutube.push(video)
                }
            } catch(error){
                console.log(error);
            }
        }

        let youtube = [];
        for(var video of ret.youtube){
            try {
                let research = await findVideoByTitleAndAuthor(video.title, video.author);
                if(research){
                    video = research;
                    youtube.push(video)
                } else {
                    youtube.push(video)
                }
            } catch(error){
                console.log(error);
            }
        }
        let result = {
            youtube: youtube,
            rutube: rutube
        }
        res.send(result); 
    });
});

router.post('/getall', function (req, res) {
    findAll().then(function (videos) {
        res.status(200).json({ success: true, videos: videos });
    }).catch(function (err) {
        res.status(500).json({ success: false, error: err });
    });
});

router.post('/getthis', function (req, res) {
    findVideoByTitleAndAuthor(req.body.title, req.body.author).then(function (video) {
        res.status(200).json({ success: true, video: video });
    }).catch(function (err) {
        res.status(500).json({ success: false, error: err });
    });
});

router.post('/add', function (req, res) {
    createVideo(req.body.video).then(function (response) {
        res.status(200).json({ success: true, video: response });
    }).catch(function (err) {
        if(err.code = 11000){
            res.status(418).json({success: false, message: "A video with this link is already in the database."})
        } else {    
            res.status(500).json({ success: false, error: err });
        }
    });

});

module.exports = router;
